{- | Byte level Sdif data structure.

- Signature: 4 bytes ("Sdif")
- Frame Size: 4 bytes (8)
- Format Version: 4 bytes (3)
- Types Version: 4 bytes (1)
-}
module Sound.Sdif.Byte.Sdif where

import qualified Data.ByteString.Lazy as B {- bytestring -}
import qualified Sound.Osc.Coding.Byte as O {- hosc -}

import Sound.Sdif.Type

-- | Check signature of Sdif byte stream (4 bytes, 0-3)
is_sdif_b :: B.ByteString -> Bool
is_sdif_b sdf =
  let sig = map (fromIntegral . fromEnum) "SDIF"
  in B.unpack (bs_section_i64 sdf 0 4) == sig

-- | Sdif version (Format Version, Type Version).
sdif_b_version :: B.ByteString -> (Int, Int)
sdif_b_version sdf =
  ( O.decode_i32 (bs_section sdf 8 12)
  , O.decode_i32 (bs_section sdf 12 16)
  )

-- | Count number of frames at Sdif byte stream.
sdif_b_frames :: B.ByteString -> Int
sdif_b_frames sdf =
  let go i j n =
        if j == 0
          then n
          else
            let sz = O.decode_i32 (bs_section sdf (i + 4) (i + 8)) + 8
            in go (i + sz) (j - sz) (n + 1)
  in go 0 (fromIntegral (B.length sdf)) 0

-- | Extract start and end indices for /n/ frames at Sdif byte stream.
sdif_b_frame_i :: B.ByteString -> Int -> [(Int, Int)]
sdif_b_frame_i sdf frame_count =
  let go i j xs n =
        if n == frame_count
          then reverse xs
          else
            let sz = O.decode_i32 (bs_section sdf (i + 4) (i + 8)) + 8
                xs' = (i, i + sz) : xs
            in go (i + sz) (j - sz) xs' (n + 1)
  in go 0 (fromIntegral (B.length sdf)) [] 0
