{- | Byte level Sdif frame data structure.

Frame Signature: 4 bytes (string)
Frame Size: 4 bytes (int) -- does not count Signature and Size fields
Time: 8 bytes (double)
StreamID: 4 bytes (int)
Matrix Count: 4 bytes (int)
-}
module Sound.Sdif.Byte.Frame where

import qualified Data.ByteString.Lazy as B {- bytestring -}
import qualified Sound.Osc.Coding.Byte as O {- hosc -}

import Sound.Sdif.Byte.Matrix
import Sound.Sdif.Type

-- | Data integrity check for Sdif frame data stream.
is_frame_b :: B.ByteString -> Bool
is_frame_b frm =
  let n = fromIntegral (B.length frm) - 8
  in frame_b_size frm == n

-- | Extract type string from Sdif frame byte stream.
frame_b_type :: B.ByteString -> String
frame_b_type frm = map (toEnum . fromIntegral) (B.unpack (bs_section_i64 frm 0 4))

-- | Extract size from Sdif frame byte stream.
frame_b_size :: B.ByteString -> Int
frame_b_size frm = O.decode_u32 (bs_section_i64 frm 4 8)

-- | Extract time stamp from Sdif frame byte stream.
frame_b_time :: B.ByteString -> Double
frame_b_time frm = O.decode_f64 (bs_section_i64 frm 8 16)

-- | Extract identifier from Sdif frame byte stream.
frame_b_id :: B.ByteString -> Int
frame_b_id frm = O.decode_i32 (bs_section_i64 frm 16 20)

-- | Extract matrix count from Sdif frame byte stream.
frame_b_matrices :: B.ByteString -> Int
frame_b_matrices frm = O.decode_i32 (bs_section_i64 frm 20 24)

-- | Extract frame data segment from Sdif frame byte stream.
frame_b_data :: B.ByteString -> B.ByteString
frame_b_data frm = bs_section_i64 frm 24 (B.length frm)

-- | Extract frame matrix /(start,end)/ indices from Sdif frame byte stream.
frame_b_matrix_i :: B.ByteString -> [(Int, Int)]
frame_b_matrix_i frm =
  let go i j xs =
        if j == 0
          then reverse xs
          else
            let h = bs_section frm i (i + matrix_b_header_size)
                sz = fromIntegral (matrix_b_storage_size h)
                xs' = (i, i + sz) : xs
            in go (i + sz) (j - sz) xs'
  in go 24 (fromIntegral (B.length frm) - 24) []
