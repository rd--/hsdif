-- | Sdif (Sound Description Interchange Format)
module Sound.Sdif.Sdif where

import qualified Data.ByteString.Lazy as B {- bytestring -}

import qualified Sound.Sdif.Byte.Sdif as S
import Sound.Sdif.Frame
import Sound.Sdif.Matrix
import Sound.Sdif.Type

{- $setup
>>> sdif <- sdif_read_file "/home/rohan/sw/hsdif/sdif/crotale.c5.res.sdif"
-}

-- | Sdif data store.
data Sdif = Sdif
  { sdif_b :: B.ByteString
  , sdif_frames :: Int
  , sdif_frame_i :: [(Int, Int)]
  , sdif_frame_c :: [Frame]
  }
  deriving (Eq, Show)

{- | Decode 'Sdif' data stream.

>>> bytes <- B.readFile "/home/rohan/sw/hsdif/sdif/crotale.c5.res.sdif"
>>> sdif_frames (decode_sdif bytes)
2
-}
decode_sdif :: B.ByteString -> Sdif
decode_sdif sdf =
  let n = S.sdif_b_frames sdf
      ix = S.sdif_b_frame_i sdf n
      frm (i, j) = decode_frame (bs_section sdf i j)
      s =
        Sdif
          { sdif_b = sdf
          , sdif_frames = n
          , sdif_frame_i = ix
          , sdif_frame_c = map frm ix
          }
  in if S.is_sdif_b sdf
      then s
      else error "decode_sdif: illegal data"

{- | Read and decode 'Sdif' from named file.

>>> sdif_frame_i sdif
[(0,16),(16,856)]
-}
sdif_read_file :: FilePath -> IO Sdif
sdif_read_file file_name = do
  b <- B.readFile file_name
  return (decode_sdif b)

-- | Extract /n/th frame data from 'Sdif'.
sdif_frame_b :: Sdif -> Int -> B.ByteString
sdif_frame_b sdf n =
  let (i, j) = sdif_frame_i sdf !! n
  in (bs_section (sdif_b sdf) i j)

{- | Extract and decode /n/th frame from 'Sdif'.
     The zero-th frame is the Sdif frame.

>>> frame_type (sdif_frame sdif 0)
"SDIF"
-}
sdif_frame :: Sdif -> Int -> Frame
sdif_frame sdf n = sdif_frame_c sdf !! n

{- | Extract and decode /j/th matrix from /i/th frame from 'Sdif'.

>>> matrix_type (sdif_matrix sdif 1 0)
"1RES"
-}
sdif_matrix :: Sdif -> Int -> Int -> Matrix
sdif_matrix sdf i = frame_matrix (sdif_frame sdf i)

{- | Run 'matrix_v' on result of 'sdif_matrix'.

>>> length (sdif_matrix_v sdif 1 0)
200
-}
sdif_matrix_v :: Sdif -> Int -> Int -> [Datum]
sdif_matrix_v sdf i = matrix_v . sdif_matrix sdf i
