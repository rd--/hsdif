# print

Various printers for SDIF data.

## print type

Print specified matrix data.

~~~~
$ hsdif-print type 1RES 4 ~/sw/hsdif/sdif/crotale.c5.res.sdif | head
Frequency,Amplitude,DecayRate,Phase
35.4568,0.0013,5.2037,0.0000
128.5985,0.0008,1.7034,0.0000
346.9722,0.0174,40.1652,0.0000
483.5544,0.0048,27.2825,0.0000
1049.2450,0.0044,0.8951,0.0000
1564.0280,0.0041,42.8474,0.0000
1756.3400,0.0003,2.6604,0.0000
3391.6660,0.0036,15.7679,0.0000
3451.8020,0.0069,6.8484,0.0000
$
~~~~

## print info

Print information about SDIF file structure.

~~~~
$ hsdif-print info ~/sw/hsdif/sdif/crotale.c5.res.sdif
frames = 2

frame-type frame-size frame-time frame-id frame-matrices
---------- ---------- ---------- -------- --------------
      SDIF          8        0.0        0              0
      1RES        832        0.0        2              1
---------- ---------- ---------- -------- --------------

matrices = 1

frame-time matrix-type   data-type type-size matrix-rows matrix-columns
---------- ----------- ----------- --------- ----------- --------------
       0.0        1RES real number         4          50              4
---------- ----------- ----------- --------- ----------- --------------
$
~~~~

## print info-ll

Print information about SDIF file structure.
Internally uses only low-level byte-stream interface.

~~~~
$ hsdif-print info-ll /home/rohan/sw/hsdif/sdif/crotale.c5.res.sdif 1 0
(856,True,2)
("1RES",832,0.0,2,1,[(24,840)])
("1RES",4,50,4,200,800,816)
$
~~~~
