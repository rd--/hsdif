# sc3

Load SDIF files and audition using SCSYNTH.

## 1RES

Load first 1RES matrix from file, and synthesize using `scsynth`.

~~~~
$ hsdif-sc3 1RES klank 0.5 5 ~/sw/hsdif/sdif/crotale.c5.res.sdif
~~~~
