import System.Environment {- base -}

import Sound.Sc3 {- hsc3 -}
import Sound.Sdif {- hsdif -}

type R = Double

maxtrix_data_co :: Fractional n => Matrix -> [[n]]
maxtrix_data_co mx =
  let f n = map datum_fractional (matrix_column mx n)
  in map f [0 .. matrix_columns mx - 1]

-- * 1Res

-- | (Frequency,Amplitude,DecayRate)
type Res = ([R], [R], [R])

load_1Res :: FilePath -> IO Res
load_1Res fn = do
  s <- sdif_read_file fn
  let fr = sdif_frame_c s
      mx = concatMap frame_matrix_c fr
  case filter ((== "1Res") . matrix_type) mx of
    [] -> error "load_1Res: no 1Res matrix"
    r : _ -> case maxtrix_data_co r of
      [cf, ca, cd, _ph] -> return (cf, ca, cd) -- (Frequency,Amplitude,DecayRate,Phase)
      [cf, ca, cd, _, _] -> return (cf, ca, cd) -- (Frequency,Amplitude,BandWidth,Saliance,Correction)
      _ -> error "load_1Res: matrix data error"

res_sc3_klank :: (R, R) -> Res -> Ugen
res_sc3_klank (df, ds) (cf, ca, cd) =
  let n = whiteNoiseId 'α' ar
      t = dustId 'β' kr (constant df)
      g = tRandId 'δ' 0.5 1 t
      s = decay2 t 0.06 0.01 * n * g
      ks = klankSpec_k cf ca (map recip cd)
  in klank s 1 0 (constant ds) ks

-- * Main

main :: IO ()
main = do
  a <- getArgs
  case a of
    ["1Res", "klank", df, ds, fn] -> load_1Res fn >>= audition . res_sc3_klank (read df, read ds)
    _ -> putStrLn "1Res klank dust-freq/hz decay/* sdif-file"

{-
let fn = "/home/rohan/sw/hsdif/sdif/crotale.c5.res.sdif"
let fn = "/home/rohan/sw/hsdif/sdif/piano.a0.res.sdif"
let fn = "/home/rohan/sw/hsdif/sdif/xylo.m6.res.sdif"
load_1Res fn >>= audition . res_sc3_klank (0.5,5)
-}
