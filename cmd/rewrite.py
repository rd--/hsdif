#!/usr/bin/env python3

import argparse
import pysdif
import sys

assert sys.version_info.major >= 3 and sys.version_info.minor >= 6

parser = argparse.ArgumentParser()
parser.add_argument('inputFile')
parser.add_argument('outputFile')
args = parser.parse_args()

inputFile = pysdif.SdifFile(args.inputFile, 'r')
outputFile = pysdif.SdifFile(args.outputFile, 'w').clone_definitions(inputFile)
for inputFrame in inputFile:
    with outputFile.new_frame(inputFrame.signature, inputFrame.time) as outputFrame:
        for matrix in inputFrame:
            data = matrix.get_data(copy=True)
            outputFrame.add_matrix(matrix.signature.decode('UTF-8'), data)
outputFile.close()
