import Control.Monad {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}
import System.Environment {- base -}

import qualified Codec.Binary.UTF8.String as Utf {- utf8-string -}
import qualified Data.ByteString.Lazy as ByteString {- bytestring -}

import qualified Music.Theory.Array.Text as Array.Text {- hmt-base -}
import qualified Music.Theory.Show as Show {- hmt-base -}

import Sound.Sdif {- hsdif -}
import qualified Sound.Sdif.Byte.Frame as F {- hsdif -}
import qualified Sound.Sdif.Byte.Matrix as M {- hsdif -}
import qualified Sound.Sdif.Byte.Sdif as S {- hsdif -}

-- * Matrix Util

vec_utf8_str :: [Datum] -> String
vec_utf8_str = Utf.decode . map datum_u8

matrix_deinterleave :: Matrix -> [[Datum]]
matrix_deinterleave m = map (matrix_row m) [0 .. matrix_rows m - 1]

matrix_real_pp :: Int -> Matrix -> [String]
matrix_real_pp k m =
  let d = matrix_deinterleave m
      t = map (map (Show.double_pp k . datum_fractional)) d
      Just (_, h) = lookup (matrix_type m) (matrix_std_types (matrix_columns m))
  in map (intercalate ",") (h : t)

matrix_pp :: Int -> Matrix -> Maybe [String]
matrix_pp k m =
  case matrix_data_type m of
    0x301 -> Just [vec_utf8_str (matrix_v m)]
    0x004 -> Just (matrix_real_pp k m)
    0x008 -> Just (matrix_real_pp k m)
    _ -> Nothing

read_mx :: FilePath -> IO [[Matrix]]
read_mx fn = do
  s <- sdif_read_file fn
  let fr = sdif_frame_c s
  return (map frame_matrix_c fr)

-- * Ix

print_ix :: Int -> Int -> Int -> FilePath -> IO ()
print_ix fr_ix mx_ix k fn = do
  mx <- read_mx fn
  let Just txt = matrix_pp k ((mx !! fr_ix) !! mx_ix)
  putStrLn (unlines txt)

-- * All

print_all :: Int -> FilePath -> IO ()
print_all k fn = do
  mx <- read_mx fn
  let txt = mapMaybe (matrix_pp k) (concat mx)
  mapM_ (putStrLn . unlines) txt

-- * Type

print_type :: String -> Int -> FilePath -> IO ()
print_type ty k fn = do
  mx <- read_mx fn
  let mx' = filter ((== ty) . matrix_type) (concat mx)
      txt = mapMaybe (matrix_pp k) mx'
  mapM_ (putStrLn . unlines) txt

-- * Info

matrix_fld :: [String]
matrix_fld = ["frame-time", "matrix-type", "data-type", "type-size", "matrix-rows", "matrix-columns"]

tm_pp :: Double -> String
tm_pp tm = if tm < 0 then "-" else Show.double_pp 4 tm

matrix_info :: Double -> Matrix -> [String]
matrix_info tm m =
  let Matrix _ ty dt nr nc _ _ _ _ = m
  in [tm_pp tm, ty, data_type_string dt, show (data_type_size dt), show nr, show nc]

frame_fld :: [String]
frame_fld = ["frame-type", "frame-size", "frame-time", "frame-id", "frame-matrices"]

frame_info :: Frame -> ([String], [[String]])
frame_info (Frame _ ty sz tm k nm _ mx) =
  ( [ty, show sz, tm_pp tm, show k, show nm]
  , map (matrix_info tm) mx
  )

print_info :: FilePath -> IO ()
print_info fn = do
  s <- sdif_read_file fn
  let i = map frame_info (sdif_frame_c s)
      fr = map fst i
      mx = concatMap snd i
      f_tbl = Array.Text.table_pp Array.Text.table_opt_simple (frame_fld : fr)
      m_tbl = Array.Text.table_pp Array.Text.table_opt_simple (matrix_fld : mx)
  mapM_ putStrLn ["frames = " ++ show (length fr), ""]
  putStrLn (unlines f_tbl)
  mapM_ putStrLn ["matrices = " ++ show (length mx), ""]
  putStrLn (unlines m_tbl)

-- * Info-Ll

print_info_ll :: FilePath -> Int -> Int -> IO ()
print_info_ll fn f_ix m_ix = do
  b <- ByteString.readFile fn
  let num_frames = S.sdif_b_frames b
  print (ByteString.length b, S.is_sdif_b b, num_frames)
  when (f_ix >= num_frames) (error "info-ll: f_ix > num_frames")
  let (i, j) = S.sdif_b_frame_i b num_frames !! f_ix
      fb = bs_section b i j
      mb = F.frame_b_data fb
      num_matrices = F.frame_b_matrices fb
  print
    ( F.frame_b_type fb
    , F.frame_b_size fb
    , F.frame_b_time fb
    , F.frame_b_id fb
    , num_matrices
    , F.frame_b_matrix_i fb
    )
  when (m_ix >= num_matrices) (error "info-ll: m_ix > fnum_matrices")
  print
    ( M.matrix_b_type mb
    , M.matrix_b_data_type mb
    , M.matrix_b_rows mb
    , M.matrix_b_columns mb
    , M.matrix_b_elements mb
    , M.matrix_b_data_size mb
    , M.matrix_b_storage_size mb
    )

-- * Main

help :: [String]
help =
  [ "print cmd arg"
  , "  all precision:int sdif-file"
  , "  info sdif-file"
  , "  info-ll sdif-file frame-index matrix-index"
  , "  ix frame:int matrix:int precision:int sdif-file"
  , "  type matrix-type:string precision:int sdif-file"
  ]

main :: IO ()
main = do
  a <- getArgs
  case a of
    ["all", k, fn] -> print_all (read k) fn
    ["info", fn] -> print_info fn
    ["info-ll", fn, f_ix, m_ix] -> print_info_ll fn (read f_ix) (read m_ix)
    ["ix", fr, mx, k, fn] -> print_ix (read fr) (read mx) (read k) fn
    ["type", ty, k, fn] -> print_type ty (read k) fn
    _ -> putStrLn (unlines help)

{-
> let fn = "/home/rohan/sw/hsdif/sdif/crotale.c5.res.sdif"
> let fn = "/home/rohan/sw/hsdif/sdif/wineglass.m6.fof.sdif"
> let fn = "/home/rohan/sw/hsdif/sdif/wineglass.m6.fof.sdif"
> let fn = "/home/rohan/sw/hsdif/sdif/xylo.m6.res.sdif"
> print_all 4 fn
> print_info fn
> print_ix 3 1 4 fn
> print_type "1FOF" 4 fn
-}
