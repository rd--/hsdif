hsdif
-----

[haskell](http://haskell.org/)
sound description interchange format ([sdif](http://sdif.sourceforge.net/))
library & cli utilities.

## cli

[sc3](?t=hsdif&e=md/sc3.md),
[print](?t=hsdif&e=md/print.md)

## ref

- Sdif Standard Frame Types: [HTML](https://cnmat.berkeley.edu/content/sdif-standard-frame-types)

© [rohan drape](http://rohandrape.net/),
  2010-2025,
  [gpl](http://gnu.org/copyleft/)

* * *

```
$ make doctest
Examples: 8  Tried: 8  Errors: 0  Failures: 0
$
```
